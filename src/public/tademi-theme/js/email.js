const sendEmail = () => {
    const name = document.getElementById('name').value;
    const email = document.getElementById('email').value;
    const phone = document.getElementById('phone').value;

    document.querySelectorAll('input.w-error').forEach(el => el.classList.toggle('w-error'));

    if (name == ''){
        document.getElementById('name').classList.toggle('w-error');
        return;
    }

    if (email == '' || !isValidEmail(email)){
        document.getElementById('email').classList.toggle('w-error');
        return;
    }

    if (phone == '' || !isPhoneNumber(phone)){
        document.getElementById('phone').classList.toggle('w-error');
        return;
    }

    const data = {
        name: name,
        email: email,
        phone: phone
    };

    fetch('/sendemail', {
        method: 'POST',
        body: JSON.stringify(data),
        headers:{
            'Content-Type': 'application/json'
        }
    }).then(res => {
        emailAnimation();
    })
    .catch(error => console.error('Error:', error));

    
}

const isValidEmail = (email) => {
    let re = /\S+@\S+\.\S+/;
    return re.test(email);
}

const isPhoneNumber = (phone) => {
    return parseInt(phone) && Number.isInteger(parseInt(phone)) && (phone.length === 9 || phone.length === 7);
}

const emailAnimation = () => {
    document.querySelector('.main-form.active').classList.toggle('active');
    document.querySelector('.envelope-wrapper').classList.toggle('active');
    document.querySelector('.envelope-wrapper.active').classList.toggle('sent');
    document.querySelector('.confirmation-message').classList.toggle('active');

    setTimeout(() => { 
        document.querySelector('.confirmation-message.active').classList.toggle('active');
        setTimeout(() => { location.reload(); }, 1000);
    }, 6000);
}