const nodemailer = require('nodemailer');
const noreplyEmail = 'tademi.noreply@gmail.com';
const noreplyPass = 'tademi.neo.noreply';
const to = 'accounts@neo.com.pe';

const transporter = nodemailer.createTransport({
    host: 'smtp.gmail.com',
    port: 465,
    secure: true,
    auth: {
        user: noreplyEmail,
        pass: noreplyPass
    }
});

export const sendEmail = (info) => {
    let mailOptions = {
        from: noreplyPass,
        to: to,
        subject: 'TADEMI Solicitud',
        html: this.getContent(info)
    };

    transporter.sendMail(mailOptions, function (error, info) {
        if (error) {
            console.log(error);
        } else {
            console.log('Email sent: ' + info.response);
        }
    });
}

export const getContent = (info) => {
    return `<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"><html xmlns="http://www.w3.org/1999/xhtml"> <head> <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/> <title>A Simple Responsive HTML Email</title> <style type="text/css"> *{margin: 0; padding: 0;}body{margin: 0; padding: 3rem; min-width: 100%!important; background-color: #3a37ea;}.main-content{width: 100%; max-width: 100vw; padding: 3rem; background-color: #05207c; box-shadow: -1px 7px 79px -6px rgba(0,0,0,0.4);}.yellow{color: #ffdc0d;}.white{color: #fff;}.md{font-size: 1rem;}p{width: 100%;}p.yellow{margin-bottom: .5rem;}</style> </head> <body> <div class="main-content"> <p class="md yellow">Datos ingresados</p><p class="md white">Nombres y Apellidos: ${info.name}</p><p class="md white">Email: ${info.email}</p><p class="md white">Teléfono: ${info.phone}</p></div></body></html>`;
}