import {AppController} from "./controller/AppController";

export const Routes = [{
    method: "get",
    route: "/",
    controller: AppController,
    action: "home"
},{
    method: "post",
    route: "/sendemail",
    controller: AppController,
    action: "email"
}];
