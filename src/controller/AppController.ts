import * as express from "express";
import {NextFunction, Request, Response} from "express";

import {sendEmail} from "../util/Email";


export class AppController{
    public home(request: Request, response: Response, next: NextFunction){
        response.render("app/__main", {
            content: 'home.ejs'
        });
    }

    public email(request: Request, response: Response, next: NextFunction){
        sendEmail(request.body);
        response.json({});
    }
}
