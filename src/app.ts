import * as express from "express";
import * as bodyParser from "body-parser";
import {Request, Response} from "express";
import {Routes} from "./routes"; 
import * as path from "path";
import * as compression from "compression";
import * as env from "dotenv";
import * as minify from "express-minify";

env.config();

const app = express();
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));

// app.use(session({
//     resave: true, 
//     secret: 'hp1101155ss', 
//     saveUninitialized: true,
//     // cookie: {
//     //     secure: false,
//     //     maxAge: 600000
//     // }
// }));

// app.use(function (req, res, next) {
//     res.header('Access-Control-Allow-Credentials', true);
//     res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
//     // res.header("Access-Control-Allow-Origin", "http://localhost");
//     res.header("Access-Control-Allow-Origin", "http://35.185.228.248");
//     res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");

//     next();
// });

if(process.env.NODE_ENV === 'production') {
    console.log('Running PRODUCTION Mode');
    app.use(compression());
    app.use(minify());
}

app.use('', express.static(path.join(__dirname, "public")));

app.set("view engine", "ejs");
app.set("views", path.join(__dirname, "views"));

// app.use(fileUpload());

Routes.forEach(route => {
    (app as any)[route.method](route.route, (req: Request, res: Response, next: Function) => {
        const result = (new (route.controller as any))[route.action](req, res, next);
        if (result instanceof Promise) {
            console.log('Promise');
            result.then(result => result !== null && result !== undefined ? res.send(result) : undefined);

        } else if (result !== null && result !== undefined) {
            console.log('Undefined');
            res.json(result);
        }
    });
});

app.use(function(req, res, next){
    res.status(404).redirect('/');
});

app.listen(9000);